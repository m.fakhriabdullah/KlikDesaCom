package com.example.muhammad.klikdesacom.model;

import java.util.List;

/**
 * Created by muhammad on 17/02/18.
 */

public class MainFeed {

    private int mGravity;
    private String mText;
    private List<FeedNews> mfeedNews;


    public MainFeed(int gravity, String text,  List<FeedNews> feedNews){
        mGravity = gravity;
        mText = text;
        mfeedNews = feedNews;
    }




    public  String getText() { return mText; }
    public int getGravity() { return  mGravity; }
    public List<FeedNews> getApps() { return mfeedNews; }
}
