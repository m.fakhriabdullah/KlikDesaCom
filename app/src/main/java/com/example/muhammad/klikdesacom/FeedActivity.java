package com.example.muhammad.klikdesacom;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.muhammad.klikdesacom.adapter.FeedAdapter;
import com.example.muhammad.klikdesacom.model.FeedNews;

import java.util.ArrayList;

public class FeedActivity extends AppCompatActivity {
    

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<FeedNews> mDataset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        mDataset = new ArrayList<>();
        for ( int i =0; i<30; i++){
            mDataset.add(new FeedNews(R.drawable.ic_launcher_background, "Judul Berita " + i,i + " Jam lalu" ));
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_feed);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new FeedAdapter(mDataset);
        mRecyclerView.setAdapter(mAdapter);
    }
}

   