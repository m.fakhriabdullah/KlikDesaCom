package com.example.muhammad.klikdesacom.model;

import com.example.muhammad.klikdesacom.model.Stats;

import java.util.List;

/**
 * Created by muhammad on 15/02/18.
 */

public class FeedNews {
    private int newsImagesFeed;
    private String titleNewsFeed;
    private String timeNewsFeed;

    public FeedNews(int NewsImageFeed, String TitleNewsFeed, String TimeNewsFeed){
        newsImagesFeed = NewsImageFeed;
        titleNewsFeed = TitleNewsFeed;
        timeNewsFeed = TimeNewsFeed;
    }


    public int getNewsImagesFeed() {
        return newsImagesFeed;
    }

    public String getTitleNewsFeed() {
        return titleNewsFeed;
    }

    public String getTimeNewsFeed() {
        return timeNewsFeed;
    }



}
