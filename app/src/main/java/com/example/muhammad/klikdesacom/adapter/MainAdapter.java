package com.example.muhammad.klikdesacom.adapter;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.muhammad.klikdesacom.ReadNewsActivity;
import com.example.muhammad.klikdesacom.model.Snap;
import com.example.muhammad.klikdesacom.model.Stats;
import com.example.muhammad.klikdesacom.R;
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muhammad on 14/02/18.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private List<Stats> mStats;
    private boolean mHorizontal;
    private boolean mPager;
    public String mType;
    public int mPosition;

    public MainAdapter(boolean horizontal, boolean pager, List<Stats> stats, int position) {
        mHorizontal = horizontal;
        mStats = stats;
        mPager = pager;
        mPosition = position;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Stats stats = mStats.get(0);
        if (stats.getType()=="Berita") {
            return new ViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_pager, parent, false));
        } else {
            return mHorizontal ? new ViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter, parent, false)) :
                    new ViewHolder(LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.adapter_vertical, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Stats stats = mStats.get(position);
        holder.nameTextView.setText(stats.getName());
        holder.imageView.setImageResource(stats.getDrawable());
        holder.cardViewStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Intent  i;
                i = new Intent(view.getContext(), ReadNewsActivity.class );
                view.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return mStats.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView nameTextView;
        public CardView cardViewStatus;
        public CardView cardViewBerita;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            nameTextView = (TextView) itemView.findViewById(R.id.textView);
            cardViewStatus = itemView.findViewById(R.id.card_status);
        }

    }

}