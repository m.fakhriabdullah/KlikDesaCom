package com.example.muhammad.klikdesacom.model;

/**
 * Created by muhammad on 14/02/18.
 */

public class Stats {
    private int mDrawable;
    private String mName;
    private String mType;




    public Stats(String type, String name, int drawable){
        mName = name;
        mDrawable = drawable;
        mType = type;
    }



    public int getDrawable() {
        return mDrawable;
    }

    public String getName() {
        return mName;
    }

    public String getType() {
        return mType;
    }





}
