package com.example.muhammad.klikdesacom.model;

import java.util.List;

/**
 * Created by muhammad on 14/02/18.
 */

public class Snap {

    private int mGravity;
    private String mText;
    private List<Stats> mStats;
    private String mNews;


    public Snap(String news, int gravity, String judul, List<Stats> stats){
        mGravity = gravity;
        mText = judul;
        mStats = stats;
        mNews = news;

    }

    public String getNews() {
        return mNews;
    }
    public  String getText() { return mText; }
    public int getGravity() { return  mGravity; }
    public List<Stats> getApps() { return mStats; }
}




